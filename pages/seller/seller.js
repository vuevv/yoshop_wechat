// pages/seller/seller.js
let App = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keywords:"",
    sellerList:[],
    inds:-1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_sellers()
  },
  // 搜索销售代表
  search(e){
    this.setData({
      keywords: e.detail.value,
      inds:-1
    })
    this.get_sellers()
  },
  // 获取搜索关键词
  getsearcgVal(e){
    this.setData({
      keywords: e.detail.value
    })
  },
  // 选择销售代表
  selectSeller(e){
    this.setData({
      inds: e.currentTarget.dataset.index
    })
  },
  get_sellers(){
    App._get('user/get_sellers', {
      keywords: this.data.keywords
    }, res => {
      this.setData({
        sellerList:res.data,
        keywords:""
      })
    })
  },
  // 确定选择
  bind_seller(){
    if(this.data.inds==-1){
      wx.showToast({
        title: '请选择销售代表',
        icon:"none"
      })
      return
    }
    wx.navigateTo({
      url: "/pages/upShop/upShop?seller_id=" + this.data.sellerList[this.data.inds].seller_id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})