let App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    _from: "",
    seller_id:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.scene != undefined) {//销售代表
      var scene = decodeURIComponent(options.scene);
      var seller_id = scene.split("=")[1];
      this.setData(seller_id)
    }else{
      if (options.from) {
        this.setData({
          _from: options.from
        })
      }
    }
  },

  /**
   * 授权登录
   */
  authorLogin: function(e) {
    let _this = this;
    if (e.detail.errMsg !== 'getUserInfo:ok') {
      return false;
    }
    if (this.data._from == 'mlogin') {
      wx.showLoading({
        title: "正在授权",
        mask: true
      });
      App._get('user/savewxuser', {
        nickName: e.detail.userInfo.nickName,
        headimgurl: e.detail.userInfo.avatarUrl,
        gender: e.detail.userInfo.gender == 1 ? 1 : 2,
        openid: wx.getStorageSync("OPENID"),
        city: e.detail.userInfo.city
      }, (res) => {
        if (res.data.seller_id == '0' && res.data.is_seller == '0') { //未绑定销售员且自己不是销售员,前往绑定销售员页面
          wx.navigateTo({
            url: '/pages/seller/seller',
          })
        }
      });
    } else {
      wx.showLoading({
        title: "正在登录",
        mask: true
      });
      // 执行微信登录
      wx.login({
        success: (res) => {
          App._get('user/get_openid', {
            code: res.code
          }, res => {
            wx.setStorageSync('token', res.data.token)
            wx.setStorageSync('OPENID', res.data.openid)
            App._get('user/savewxuser', {
              nickName: e.detail.userInfo.nickName,
              headimgurl: e.detail.userInfo.avatarUrl,
              gender: e.detail.userInfo.gender == 1 ? 1 : 2,
              openid: wx.getStorageSync("OPENID"),
              city: e.detail.userInfo.city,
              seller_id: this.data.seller_id
            }, (res) => {
              if (res.data.mobile_verified == '0') { //手机号未绑定
                wx.navigateTo({
                  url: '/pages/bindPhone/bindPhone',
                })
              } else {
                if (res.data.seller_id == '0' && res.data.is_seller == '0') { //未绑定销售员且自己不是销售员,前往绑定销售员页面
                  wx.navigateTo({
                    url: '/pages/seller/seller',
                  })
                } else {
                  wx.setStorageSync("userInfo", res.data)
                  if (!wx.getStorageSync("Back")) {
                    wx.switchTab({
                      url: "/pages/index/index",
                    })
                  } else {
                    wx.setStorageSync('loginBack', true)
                    wx.setStorageSync('Back', false)
                    wx.navigateBack({
                      delta: 2
                    })
                  }
                }
              }

            });
          })
        }
      });
    }

  },

  /**
   * 授权成功 跳转回原页面
   */
  navigateBack: function() {
    wx.navigateBack();
    // let currentPage = wx.getStorageSync('currentPage');
    // wx.redirectTo({
    //   url: '/' + currentPage.route + '?' + App.urlEncode(currentPage.options)
    // });
  },

})