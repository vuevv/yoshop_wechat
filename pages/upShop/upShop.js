// pages/upShop/u pShop.js
let App = getApp();
import siteInfo from '../../siteinfo.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    seller_id: "",
    imgList: [],
    location:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      seller_id: options.seller_id
    })
  },
  del(e){
    this.data.imgList.splice(e.currentTarget.dataset.index,1)
    this.setData({
      imgList: this.data.imgList
    })
  },
  // 获取用户地址
  getlocation(e){
    this.setData({
      location: e.detail.value
    })
  },
  // 选择照片上传
  selectImage() {
    wx.chooseImage({
      count: 6 - this.data.imgList.length,
      sizeType: ['original'],
      sourceType: ['album', 'camera'],
      success: (res) => {
        // tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths
        // if (tempFilePaths.length>0){
        //   wx.showLoading({
        //     title: '上传中...',
        //   })
        // }
        // setTimeout(()=>{

        // },3000)
        for (let i = 0; i < res.tempFilePaths.length; i++) {
          wx.uploadFile({
            url: siteInfo.siteroot +'/api/fileupload/uploadimg',
            filePath: tempFilePaths[i],
            name: 'upfile',
            formData: {
              'token': wx.getStorageSync('token')
            },
            success:(res)=>{
              let data = JSON.parse(res.data)
              this.setData({
                imgList: this.data.imgList.concat(data.data)
              })
            }
          })
        }
      }
    })
  },
  // 提交审核
  submit() {
    if (this.data.location.length==''){
      wx.showToast({
        title: '请输入门店地址（必填）',
        icon:"none"
      })
      return
    }
    App._get('user/bind_seller', {
      seller_id: this.data.seller_id,
      store_imgs: this.data.imgList.join(','),
      location: this.data.location
    }, res => {
      App._get('user/detail', {},  (res)=>{
        wx.setStorageSync('userInfo', res.data.userInfo)
        wx.switchTab({
          url: "/pages/user/index",
        })
      });
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})