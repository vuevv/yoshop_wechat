const App = getApp();

Page({
  data: {
    // 搜索框样式
    searchColor: "rgba(0,0,0,0.4)",
    searchSize: "15",
    searchName: "搜索商品",
    classList:[],
    // 列表高度
    scrollHeight: 0,

    // 一级分类：指针
    curNav: true,
    curIndex: 0,

    // 分类列表
    list: [],

    // show
    notcont: false
  },

  onLoad: function() {
    let _this = this;
    // 设置分类列表高度
    _this.setListHeight();
    // 获取分类列表
    _this.getCategoryList();
  },
  onShow(){
    if (wx.getStorageSync("token")){
      this.getTotal_goods_num()
    }
  },
  /**
   * 设置分类列表高度
   */
  setListHeight: function() {
    let _this = this;
    wx.getSystemInfo({
      success: function(res) {
        _this.setData({
          scrollHeight: res.windowHeight - 47,
        });
      }
    });
  },
  getTotal_goods_num() {
    App._get('cart/total_goods_num', {}, (res) => {
      if (res.data == 0) {
        wx.hideTabBarRedDot({
          index: 2,
        })
      } else {
        wx.setTabBarBadge({
          index: 2,
          text: JSON.stringify(res.data),
        })
      }
    })
  },
  /**
   * 获取分类列表
   */
  getCategoryList: function() {
    let _this = this;
	App._get('home/index', {}, (result)=>{
	  this.setData({
	    list: result.data.category,
		curNav: result.data.category.length > 0 ? result.data.category[0].id : true,
		notcont:!result.data.category.length
	  });
    this.getsub_category(result.data.category[0].id)
	});
  },
  // 获取子分类列表
  getsub_category(pid) {
    App._get('category/sub_category', {
      pid: pid
    }, res => {
      this.setData({
        classList: res.data
      })
    })
  },
  /**
   * 一级分类：选中分类
   */
  selectNav: function(t) {
    let curNav = t.target.dataset.id,
      curIndex = parseInt(t.target.dataset.index);
    this.setData({
      curNav,
      curIndex,
      scrollTop: 0
    });
    
    this.getsub_category(curNav)
  },

  /**
   * 设置分享内容
   */
  onShareAppMessage: function() {
    return {
      title: "全部分类",
      path: "/pages/category/index"
    };
  }

});