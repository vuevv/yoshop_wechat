let App = getApp();

Page({
  data: {
    searchColor: "rgba(0,0,0,0.4)",
    searchSize: "15",
    searchName: "搜索商品",

    scrollHeight: null,
    showView: false,
    arrange: "",

    sortType: 'all',    // 排序类型
    sortPrice: false,   // 价格从低到高

    option: {},
    list: {},

    noList: true,
    no_more: false,

    page: 1,
    offon:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    let _this = this;

    // 设置商品列表高度
    _this.setListHeight();
    console.log(option)
    // 记录option
    _this.setData({ option}, function () {
      // 获取商品列表
      _this.getGoodsList();
    });

  },

  /**
   * 获取商品列表
   */
  getGoodsList: function () {
    let _this = this;
    App._get('search/search', {
      page: this.data.page,
      sort: 0,
      category_id: this.data.option.id||"",
      keywords: this.data.option.search,
    },  (res)=>{
      if (this.data.page == 1) {
        this.setData({
          list: res.data
        })
      }else{
        if(res.data.length==0){
          wx.showToast({
            title: '没有更多数据',
            icon:"none"
          })
        }else{
          this.setData({
            list:this.data.list.concat(res.data),
            offon:true
          })
        }
      }
    });
  },
// 返回
  back(){
    wx.navigateBack()
  },
  /**
   * 设置商品列表高度
   */
  setListHeight: function () {
    let _this = this;
    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          scrollHeight: res.windowHeight - 90,
        });
      }
    });
  },

  /**
   * 切换排序方式
   */
  switchSortType: function (e) {
    let _this = this
      , newSortType = e.currentTarget.dataset.type
      , newSortPrice = newSortType === 'price' ? !_this.data.sortPrice : true;

    _this.setData({
      list: {},
      page: 1,
      offon:true,
      sortType: newSortType,
      sortPrice: newSortPrice
    }, function () {
      // 获取商品列表
      _this.getGoodsList();
    });
  },

  /**
   * 跳转筛选
   */
  toSynthesize: function (t) {
    wx.navigateTo({
      url: "../category/screen?objectId="
    });
  },

  /**
   * 切换列表显示方式
   */
  onChangeShowState: function () {
    let _this = this;
    _this.setData({
      showView: !_this.data.showView,
      arrange: _this.data.arrange ? "" : "arrange"
    });
  },

  /**
   * 下拉到底加载数据
   */
  bindDownLoad: function () {
    if (this.data.offon) {
      this.setData({
        offon: false,
        page: this.data.page += 1
      })
      this.getGoodsList();
    }
  },

  /**
   * 设置分享内容
   */
  onShareAppMessage: function () {
    return {
      title: "全部分类",
      desc: "",
      path: "/pages/category/index"
    };
  },

});
