// pages/mlogin/mlogin.js
let App = getApp();
let t = null
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mobile: '',
    code: '',
    time: 60,
    offon: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (wx.getStorageSync('token') && (wx.getStorageSync('userInfo').seller_id && wx.getStorageSync('userInfo').seller_id != 0)) {
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  // 微信登录
  toWechat_login() {
    wx.navigateTo({
      url: '/pages/login/login'
    })
  },
  // 手机号登录
  loginByPhone() {
    if (this.data.mobile.length != 11) {
      wx.showToast({
        title: '请填写正确的手机号',
        icon: "none"
      })
      return
    } else if (this.data.code.length < 6) {
      wx.showToast({
        title: '请填写正确的验证码',
        icon: "none"
      })
      return
    } else {
      App._get('user/bind_phone', {
        phone: this.data.mobile,
        code: this.data.code
      }, (res) => {
        wx.navigateTo({
          url: '/pages/seller/seller',
        })
      })
    }

  },
  inputChange(e) {
    const key = e.currentTarget.dataset.key;
    this.setData({
      [key]: e.detail.value
    })
  },
  // 发送验证码
  sendMessage() {
    if (this.data.time != 60 || !this.data.offon) {
      return
    }
    this.setData({
      offon: false
    })
    if (this.data.mobile.length != 11) {
      wx.showToast({
        title: '请填写正确的手机号',
        icon: "none"
      })
      this.setData({
        offon: true
      })
      return
    }
    App._get('user/send_code', {
      phone: this.data.mobile
    }, (result) => {
      t = setInterval(() => {
        if (this.data.time == 0) {
          this.setData({
            offon: true,
            time: 60
          })
          clearInterval(t)
          return
        }
        this.setData({
          time: this.data.time -= 1
        })
      }, 1000)
    });

    this.setData({
      time: this.data.time -= 1
    })
    return


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})