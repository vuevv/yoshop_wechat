let App = getApp();

Page({
  data: {
    searchColor: "rgba(0,0,0,0.4)",
    searchSize: "15",
    searchName: "搜索商品",

    scrollHeight: null,
    showView: false,
    arrange: "",

    sortType: 'all',    // 排序类型
    sortPrice: false,   // 价格从低到高

    option: {
      id:""
    },
    list: {},

    noList: true,
    no_more: false,

    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    wx.hideShareMenu()
    // 设置商品列表高度
    this.setListHeight();

    // 记录option
    this.setData({ option },  ()=>{
      // 获取商品列表
      this.getGoodsList();
    });
  },

  /**
   * 获取商品列表
   */
  getGoodsList: function () {
    let _this = this;
    var sort = "";
    console.log(this.data.sortType)
    if (this.data.sortType == 'price'){
      if (this.data.sortPrice){
        sort = "price_asc"
      }else{
        sort = "price_desc"
      }
    }
    if (this.data.sortType =="sales"){
      sort = "sale_desc"
    }
    if (this.data.sortType =="all"){
      sort = "all_0"
    }
    console.log(sort)
    App._get('search/search', {
      page: this.data.page,
      sort: sort,
      category_id: this.data.option.id,
      page: this.data.page,
    }, (res) => {
      if(this.data.page==1){
        this.setData({
          list:res.data
        })
      } else {
        if (res.data.length == 0) {
          wx.showToast({
            title: '没有更多数据',
            icon: "none"
          })
        } else {
          this.setData({
            list: this.data.list.concat(res.data),
            offon: true
          })
        }
      }
    });
  },

  /**
   * 设置商品列表高度
   */
  setListHeight: function () {
    let _this = this;
    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          scrollHeight: res.windowHeight,
        });
      }
    });
  },

  /**
   * 切换排序方式
   */
  switchSortType: function (e) {
    console.log(e.currentTarget.dataset.type)
    console.log(this.data.sortPrice)//true从高到底  false从低到高
    let _this = this
      , newSortType = e.currentTarget.dataset.type
      , newSortPrice = newSortType === 'price' ? !_this.data.sortPrice : true;

    _this.setData({
      list: {},
      page: 1,
      offon: true,
      sortType: newSortType,
      sortPrice: newSortPrice
    }, function () {
      // 获取商品列表
      _this.getGoodsList();
    });
  },

  /**
   * 跳转筛选
   */
  toSynthesize: function (t) {
    wx.navigateTo({
      url: "../category/screen?objectId="
    });
  },

  /**
   * 切换列表显示方式
   */
  onChangeShowState: function () {
    let _this = this;
    _this.setData({
      showView: !_this.data.showView,
      arrange: _this.data.arrange ? "" : "arrange"
    });
  },

  /**
   * 下拉到底加载数据
   */
  bindDownLoad: function () {
    if (this.data.offon) {
      this.setData({
        offon: false,
        page: this.data.page += 1
      })
      this.getGoodsList();
    }
  },

  /**
   * 设置分享内容
   */
  onShareAppMessage: function () {
    return {
      title: "全部分类",
      desc: "",
      path: "/pages/category/index"
    };
  },

});
