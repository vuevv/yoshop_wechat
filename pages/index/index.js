let App = getApp();

Page({
  data: {
    searchColor: "rgba(0,0,0,0.4)",
    searchSize: "15",
    searchName: "搜索商品",
    bannerList: [],
    current: 0,
    classList: [],
    good_list: [],
    showOpenid:0
  },

  onLoad: function() {
    wx.hideTabBar()
    if (wx.getStorageSync("token")) {
      wx.login({
        success: res => {
          App._get('user/get_openid', {
            code: res.code
          }, res => {
            this.setData({
              showOpenid:1
            })
            wx.setStorageSync('token', res.data.token)
            wx.showTabBar()
            this.getTotal_goods_num()
            App._get('user/detail', {}, function (res) {
              wx.setStorageSync('userInfo', res.data.userInfo)
            });
          })
        }
      })
    }
    // 获取首页数据
    this.getIndexData();
    this.goodsList()
    //  else {//用户未登录前往登录
    //   wx.setStorageSync("Back", true)
    //   wx.navigateTo({
    //     url: '/pages/mlogin/mlogin?from=goods',
    //   })
    // }
  },
  getTotal_goods_num(){
    if (!wx.getStorageSync("token")){
      wx.showTabBar()
      return
    }
    App._get('cart/total_goods_num', {},  (res)=>{
      if(res.data==0){
        wx.hideTabBarRedDot({
          index: 2,
        })
      }else{
        wx.setTabBarBadge({
          index: 2,
          text: JSON.stringify(res.data),
        })
      }
    })
  },
  toPage(e){
    let inds = e.currentTarget.dataset.index
    if (this.data.bannerList[inds].types=='0'){//未设置
      return
    } else if (this.data.bannerList[inds].types == '1'){//商品详情
      wx.navigateTo({
        url: this.data.bannerList[inds].link,
      })
      return
    }else{//关于我们
      wx.navigateTo({
        url: '/pages/user/about',
      })
    }
  },
  onShow() {
    
    return
    if (this.data.showOpenid==1){
      wx.login({
        success: res => {
          App._get('user/get_openid', {
            code: res.code
          }, res => {
            wx.setStorageSync('token', res.data.token)
            
          })
        }
      })
    }
  },
  /**
   * 获取首页数据
   */
  getIndexData: function() {
    App._get('home/index', {}, (result)=>{
      this.setData({
        bannerList: result.data.banner,
        classList: result.data.category
      });
    });
  },
  goodsList(){
    App._get('home/goods_list', {}, (result) => {
      this.setData({
        good_list: result.data
      });
      wx.stopPullDownRefresh()
    });
  },
  // 前往商品详情
  toGoods_detail(e){
    this.add_form_id(e.detail.formId);
    if (e.currentTarget.dataset.offon == '1') {
      wx.navigateTo({
        url: '/pages/goods/index?id=' + e.currentTarget.dataset.id,
      })
    }
  },
  // 前往分类
  toClassify(e){
    this.add_form_id(e.detail.formId);
    if (e.currentTarget.dataset.offon == '1') {
      wx.navigateTo({
        url:`/pages/classifyList/classifyList?id=${e.currentTarget.dataset.id}&&name=${e.currentTarget.dataset.name}`,
      })
    }
  },
  add_form_id(formId){
    if (wx.getStorageSync("token")){
      App._get('formid/setformid', {
        formid: formId
      }, (result) => {
      });
    }
  },
  onShareAppMessage: function() {
    return {
      title: "生鲜配送",
      desc: "",
      path: "/pages/index/index"
    };
  },
  onPullDownRefresh(){
    wx.login({
      success: res => {
        App._get('user/get_openid', {
          code: res.code
        }, res => {
          wx.setStorageSync('token', res.data.token)
          // 获取首页数据
          this.getIndexData();
          this.goodsList()
        })
      }
    })
  }
});