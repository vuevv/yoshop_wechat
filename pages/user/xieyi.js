let App = getApp(),
  wxParse = require("../../wxParse/wxParse.js");;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    nodes:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取帮助列表
    this.getHelpList();
  },

  /**
   * 获取帮助列表
   */
  getHelpList: function () {
    App._get('user/server_xy', {}, res => {
      if (res.data.length > 0) {
        this.setData({
          nodes:res.data
        })
        // wxParse.wxParse('content', 'html', res.data, this, 0);
      }
    });
  },

})