let App = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    orderCount: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },
  toSeller(){
    wx.navigateTo({
      url: '/pages/seller/seller',
    })
  },
  getTotal_goods_num() {
    App._get('cart/total_goods_num', {}, (res) => {
      if (res.data == 0) {
        wx.hideTabBarRedDot({
          index: 2,
        })
      } else {
        wx.setTabBarBadge({
          index: 2,
          text: JSON.stringify(res.data),
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (wx.getStorageSync("token")){
      if(wx.getStorageSync("userInfo")){
        if (wx.getStorageSync("userInfo").seller_id == '0') { //未绑定销售员
          wx.navigateTo({
            url: '/pages/seller/seller',
          })
        } else {
          this.getTotal_goods_num()
          this.getUserDetail();
        }
      } else {
        //用户未登录前往登录
        wx.setStorageSync("Back", true)
        wx.navigateTo({
          url: '/pages/mlogin/mlogin?from=goods',
        })
        return
      }
    }else{
      //用户未登录前往登录
      wx.setStorageSync("Back", true)
      wx.navigateTo({
        url: '/pages/mlogin/mlogin?from=goods',
      })
      return
    }
  },

  /**
   * 获取当前用户信息
   */
  getUserDetail: function() {
    let _this = this;
    App._get('user/detail', {}, function (res) {
      _this.setData(res.data);
      wx.setStorageSync('userInfo', res.data.userInfo)
    });
  },
  /**
   * 订单导航跳转
   */
  onTargetOrder(e) {
    // 记录formid
    // App.saveFormId(e.detail.formId);
    let urls = {
      payment: '/pages/order/index?type=payment',
      delivery: '/pages/order/index?type=delivery',
      done: '/pages/order/index?type=done',
      cancel: '/pages/order/index?type=cancel',
    };
    // 转跳指定的页面
    wx.navigateTo({
      url: urls[e.currentTarget.dataset.type]
    })
  },

  /**
   * 菜单列表导航跳转
   */
  onTargetMenus(e) {
    // 记录formId
    // App.saveFormId(e.detail.formId);
    wx.navigateTo({
      url: '/' + e.currentTarget.dataset.url
    })
  },

})