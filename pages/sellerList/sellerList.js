// pages/sellerList/sellerList.js
let App = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page:1,
    offon:true,
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUser_list()
  },
  // 获取我下级的用户
  getUser_list(){
    let _this = this;
    App._get('seller/get_kehu_list', {
      page:this.data.page
    },  (res)=>{
     if(res.data.page==1){
        this.setData({
          list: res.data
        })
     }else{
       if(res.data.length==0){
         wx.showToast({
           title: '没有更多数据...',
           icon:"none"
         })
       }else{
         this.setData({
           list:this.data.list.concat(res.data),
           offon:true
         })
       }
     }
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.offon){
      this.setData({
        offon:false,
        page:this.data.page+=1
      })
      this.getUser_list()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})