let App = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		pid: "",
    classList:[]
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		this.setData({
			pid: options.id
		})
		wx.setNavigationBarTitle({
			title:options.name
		})
		this.getsub_category()
	},
	// 获取分类列表
	getsub_category() {
		App._get('category/sub_category', {
			pid: this.data.pid
		}, res => {
      this.setData({
        classList:res.data
      })
		})
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},
	// 前往分类
	toClassify(e) {
		this.add_form_id(e.detail.formId);
		if (e.currentTarget.dataset.offon == '1') {
			wx.navigateTo({
				url: '/pages/classify/classify?id=' + e.currentTarget.dataset.id,
			})
		}
	},
	add_form_id(formId) {
    if (wx.getStorageSync("token")){
      App._get('formid/setformid', {
        formid: formId
      }, (result) => { });
    }
	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
